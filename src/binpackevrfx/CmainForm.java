/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binpackevrfx;

import evrLib.gc.Tprofile;
import evrLib.binpack.Tbinpacker;
import esyoLib.Formsfx.Tcontroller;
import esyoLib.Formsfx.Tform;
import esyoLib.Files.TfileUtils;
import esyoLib.Formsfx.TformException;
import evrLib.gc.Tconfiguration;
import evrLib.gc.TconfigurationPlat;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import evrLib.gc.Tpotelet;
import java.util.HashMap;
import java.util.function.BiConsumer;

/**
 * FXML Controller class Controleur de la fenêtre principale
 *
 * @author rpetit
 */
public class CmainForm extends Tcontroller implements Initializable {

    //
    private double LGBARRE = 0;
    //
    private String Fnom = "";
    //
    private HashMap<String, TvueBinpack> Fvues = new HashMap();
    //
    private ObservableList<File> Ffiles = FXCollections.observableArrayList();
    //
    private ObservableList<Tpotelet> Fpotelets = FXCollections.observableArrayList();
    //
    private ObservableList<Tprofile> Fmc = FXCollections.observableArrayList();
    //
    private ObservableList<Tprofile> Ftrav = FXCollections.observableArrayList();
    //
    private ObservableList<Tprofile> Fbal = FXCollections.observableArrayList();
    //
    private ObservableList<Tprofile> Fcache = FXCollections.observableArrayList();
    //
    private String Ffolder;
    //
    private String FselectedDirectory = "";
    //
    private String Fgamme = "";
    //éléments de l'IHM
    @FXML
    private ListView<File> fileList;
    @FXML
    private TextField margeMC;
    @FXML
    private TextField margeTrav;
    @FXML
    private MenuItem quitterMenu;
    @FXML
    private MenuItem aproposMenu;
    @FXML
    private TextField margeBal;

    /**
     * Initializes the controller class.
     *
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Possibilité de choix multiple
        fileList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //Défini le onClick
        fileList.setOnMouseClicked((Event event) -> {
            try {
                calculer(null);
            } catch (Exception ex) {
                Logger.getLogger(CmainForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        //Associer à Ffiles
        fileList.setItems(Ffiles);
    }

    /**
     * Rafraîchit l'affichage
     */
    @Override
    public void refresh() {

    }

    /**
     * Initialisation différée
     */
    @Override
    public void manualIni() {

    }

    @FXML
    private void selectFolder(ActionEvent event) throws IOException, Exception {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choisir un dossier de chantier...");
        try {
            File defaultDirectory = new File(BinpackEVRfx.options.getChantierPath("Chrome"));
            if (defaultDirectory.isDirectory()) {
                chooser.setInitialDirectory(defaultDirectory);
            }
        } catch (Exception ex) {

        }
        File selectedDirectory = chooser.showDialog(getForm().getStage());
        if (selectedDirectory != null) {
            FselectedDirectory = selectedDirectory.getPath();
            if (selectedDirectory.isDirectory()) {
                Ffolder = TfileUtils.folderName(selectedDirectory.getPath(), "\\\\");
                Ffiles.clear();
                Files.walk(Paths.get(selectedDirectory.getPath()),1).forEach(filePath -> {
                    try{
                    if (Files.isRegularFile(filePath) && TfileUtils.getExtension(filePath.toFile().getName()).equals(".pla")) {
                        Ffiles.add(filePath.toFile());

                    }
                    }catch(Exception ex){
                        
                    }
                });
                fileList.getSelectionModel().selectAll();
                calculer(null);
            }
        }
    }

    @FXML
    private void exportDecoupes(ActionEvent event) {
        if (Fvues.size() <= 0) {
            return;
        }
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choisir un dossier pour exporter...");
        try {
            File defaultDirectory = new File(FselectedDirectory);
            chooser.setInitialDirectory(defaultDirectory);
        } catch (Exception ex) {

        }
        File selectedDirectory = chooser.showDialog(getForm().getStage());
        Fvues.forEach((Object t, Object u) -> {
            try {
                ((TvueBinpack) u).exportAsPng(selectedDirectory.getPath() + "\\" + Ffolder + "-Découpes " + (String) t + ".png");
            } catch (IOException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
                BinpackEVRfx.application.showError(ex);
            }
        });
    }

    //
    private void listMorceaux(ObservableList<File> l) throws Exception {
        if (Fvues.size() > 0) {
            Fvues.forEach((Object t, Object u) -> {
                ((TvueBinpack) u).close();
            });
            Fvues.clear();
        }
        //Crée la liste complète des potelets/mc/traverses/balustres/caches selon la sélèction des portions
        boolean ad = false;
        boolean ag = false;
        Fpotelets.clear();
        Fmc.clear();
        Ftrav.clear();
        Fbal.clear();
        Fcache.clear();
        //Marge depuis l'UI
        double MARGEMC = Double.parseDouble(margeMC.getText());
        double MARGETRAV = Double.parseDouble(margeTrav.getText());
        double MARGEBAL = Double.parseDouble(margeBal.getText());
        //
        //Pour chaque fichier de configuration choisis
        for (File s : l) {
            //configuration 'plat' vide
            Tconfiguration bpf = new TconfigurationPlat(false);
            //ouvre le fichier
            bpf.loadFromFile(s.getPath());
            //vérifie que c'est la même gamme que les autres
            if (!Fgamme.isEmpty() && !bpf.getGamme().getNom().equals(Fgamme)) {
                throw new Exception("Une ou plusieurs des portions choisies ne sont pas de la même gamme que les autres! (" + Fgamme + " vs " + bpf.getGamme() + ")");
            }
            //La gamme actuelle est celle de la dernière config ouverte
            Fgamme = bpf.getGamme().getNom();
            //Nom de la config ouverte
            Fnom = bpf.getNom();
            //Récupère l'id à la fin du nom de fichier
            String[] ts = TfileUtils.removeExtension(s.getName()).split("\\-");
            //liste les potelets de la config
            if (!bpf.getSansPotelet()) {
                for (int i = 0; i < bpf.getPotelets().size(); i++) {
                    //potelet (#i)
                    //----------
                    Tpotelet p = (Tpotelet) bpf.getPotelets().get(i);
                    //type du potelet
                    //System.out.println("Potelet #" + i + ": type=" + p.getType());
                    //si le potelet n'est pas actif, je ne le compte pas
                    if (!p.getActif()) {
                        continue;
                    }
                    //angle gauche ou droit?
                    if (ad && (p.getType().equals("AG") || p.getType().equals("AD"))) {
                        System.out.println("Angle déjà là, ne met plas l'angle gauche");
                        ad = false;
                        continue;
                    }
                    //angle gauche ou droit?
                    if (ag && (p.getType().equals("AD") || p.getType().equals("AG"))) {
                        System.out.println("Angle déjà là, ne met plas l'angle droit");
                        ag = false;
                        continue;
                    }
                    //angle droit?
                    if (!ad && p.getType().equals("AD")) {
                        ad = true;
                    }
                    //angle gauche?
                    if (!ag && p.getType().equals("AG")) {
                        ag = true;
                    }
                    //défini le numéro de portion du potelet (id à la fin du nom de fichier)
                    p.setPortion(ts[ts.length - 1]);
                    //défini la référence du profilé potelet -> déjà dans le fichier xml?
                    p.setRef(bpf.getPotRef());
                    //défini la famille du potelet
                    p.setFamille("Potelet");
                    //défini la longueur de barre selon celle de la config
                    LGBARRE = bpf.FLGBARRE;
                    //ajoute le potelet à l aliste
                    Fpotelets.add(p);
                    //CACHES
                    //------
                    //si longueur>0 et que la ref n'est pas vide ajoute le morceau (longueur,ref,portion#)
                    if (((Tpotelet) bpf.getPotelets().get(i)).getLongueur() - 100 > 0 && !bpf.getCacheRef().isEmpty()) {
                        Fcache.add(new Tprofile(((Tpotelet) bpf.getPotelets().get(i)).getLongueur() - 100, bpf.getCacheRef(), ts[ts.length - 1], "Cache"));
                    }
                }
            }
            //Morceaux de mc
            if (bpf.getMainCourante()) {
                //si longueur>0 et que la ref n'est pas vide ajoute le morceau (longueur,ref,portion#)
                if (bpf.getLongueur() + MARGEMC > 0 && !bpf.getMCRef().isEmpty()) {
                    Fmc.add(new Tprofile(bpf.getLongueur() + MARGEMC, bpf.getMCRef(), ts[ts.length - 1], "MC"));
                }
            }
            //Morceaux de traverses
            double nb = 0;
            if (Fgamme.equals("Monoprofil") && bpf.getIsBal()) {
                nb = 2;
            } else {
                nb = bpf.getNbrTraverses();
            }
            if (nb > 0 && !bpf.getTraversesCable()) {
                for (int i = 0; i < nb; i++) {
                    //si longueur>0 et que la ref n'est pas vide ajoute le morceau (longueur,ref,portion#)
                    if (bpf.getLongueur() + MARGETRAV > 0 && !bpf.getTravRef().isEmpty()) {
                        Ftrav.add(new Tprofile(bpf.getLongueur() + MARGETRAV, bpf.getTravRef(), ts[ts.length - 1], "Traverse"));
                    }
                }
            }
            //Morceaux de balustres
            if (bpf.getIsBal() && bpf.getCalculatedTotBal() > 0) {
                for (int i = 0; i < bpf.getCalculatedTotBal(); i++) {
                    //si longueur>0 et que la ref n'est pas vide ajoute le morceau (longueur,ref,portion#)
                    if (bpf.getHauteurBal() + MARGEBAL > 0 && !bpf.getBalRef().isEmpty()) {
                        Fbal.add(new Tprofile(bpf.getHauteurBal() + MARGEBAL, bpf.getBalRef(), ts[ts.length - 1], "Balustre"));
                    }
                }
            }

        }
        //Table des références de profilé et de leurs morceaux
        HashMap<String, String> refs = new HashMap();
        //potelets
        for (Tprofile p : Fpotelets) {
            addMorceauRef(refs, p, 1, "", ((Tpotelet) p).getCode());
        }
        //caches
        for (Tprofile p : Fcache) {
            addMorceauRef(refs, p, 2, "", "");
        }
        //mc
        for (Tprofile p : Fmc) {
            addMorceauRef(refs, p, 1, "", "");
        }
        //trav
        for (Tprofile p : Ftrav) {
            addMorceauRef(refs, p, 1, "", "");
        }
        //bal
        for (Tprofile p : Fbal) {
            addMorceauRef(refs, p, 1, "", "");
        }
        //

        refs.forEach(new BiConsumer() {
            @Override
            public void accept(Object t, Object u) {
                bp((String) t, (String) u);
            }
        });
    }

    @FXML
    private void openFolder() {
        if (FselectedDirectory.isEmpty()) {
            return;
        }
        File f = new File(FselectedDirectory);
        if (f.isDirectory()) {
            try {
                Desktop.getDesktop().open(f);
            } catch (IOException ex) {

            }
        }
    }

    @FXML
    private void calculer(ActionEvent event) throws Exception {
        if (fileList.getSelectionModel().getSelectedItems().size() <= 0) {
            return;
        }
        listMorceaux(fileList.getSelectionModel().getSelectedItems());
    }

    @FXML
    private void quitter(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void apropos(ActionEvent event) {
        //crée la fenêtre d'inputs
        Tform f = BinpackEVRfx.application.createForm("", "binpackevrfx/aProposForm.fxml", null).setTitle("A propos de Binpacker");
        try {
            //lie l'objet à la fenêtre
            //f.setObj(FselectedProjet);
            //met à jour la fenêtre,affiche la fenêtre,place le fenêtre en 1er
            f.refresh().show().toFront();
        } catch (Exception ex) {
            Logger.getLogger(CmainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //
    private void addMorceauRef(HashMap<String, String> refs, Tprofile p, int coeff, String forceRef, String codeMorceau) {
        String ls;
        String type = "";
        //type si c'est un profilé de potelet
        try {
            type = ((Tpotelet) p).getType();
        } catch (Exception e) {

        }
        //ref du profilé
        String ref = p.getRef();
        //ou ref forcée
        if (!forceRef.isEmpty()) {
            ref = forceRef;
        }
        //si la liste des références de barre contient déjà la ref du profilé à utiliser
        if (refs.containsKey(ref)) {
            //je récupère la liste
            ls = refs.getOrDefault(ref, "?");
            //j'ajoute mon morceau
            ls += p.getLongueur() + "=" + coeff + "(" + type + "," + p.getPortion() + "," + p.getFamille() + "," + codeMorceau + ")\n";
            //je remplace
            refs.put(p.getRef(), ls);
        } else {
            //sinon
            //je crée le 1er morceau
            ls = p.getLongueur() + "=" + coeff + "(" + type + "," + p.getPortion() + "," + p.getFamille() + "," + codeMorceau + ")\n";
            //je défini le morceau pour cette ref
            refs.put(ref, ls);
        }
    }

    //
    private void bp(String ref, String liste) {
        System.out.println("Vue: " + ref);
        //binpack
        Tbinpacker b = new Tbinpacker();
        b.setNom(Ffolder + "-" + ref);
        b.setLGBARRE(LGBARRE);

        b.loadStrings(liste);
        b.bfd();
        b.binpack();
        //dessine les barres dans une fenêtre
        int i = 0;
        while (i < b.getBarres().size()) {
            TvueBinpack vue = null;
            try {
                vue = new TvueBinpack(ref,b);
                vue.setRANGE(i, 26);
                vue.createForm();               
                vue.draw();
                Fvues.put(ref + "-" + i, vue);
                i += 26;                
            } catch (Exception ex) {
                BinpackEVRfx.application.showError(ex);
            }
        }
    }

    /**
     * Ouvre le dossier puis calcule
     * @param s Chemin du dossier
     * @throws IOException Erreur de lecture
     */
    public void open(String s) throws IOException, Exception {
        File selectedDirectory = new File(s);
        FselectedDirectory = selectedDirectory.getPath();
        //BinpackEVRfx.application.showError("Open", s+"\n"+FselectedDirectory);
        Ffolder = TfileUtils.folderName(selectedDirectory.getPath(), "\\\\");
        Files.walk(Paths.get(selectedDirectory.getPath())).forEach(filePath -> {
            if (Files.isRegularFile(filePath) && TfileUtils.getExtension(filePath.toFile().getName()).equals(".pla")) {
                Ffiles.add(filePath.toFile());
                //System.out.println(filePath);
            }
        });
        fileList.getSelectionModel().selectAll();
        calculer(null);        
    }

}
