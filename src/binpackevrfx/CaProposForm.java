/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package binpackevrfx;

import esyoLib.Formsfx.Tcontroller;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 * Contrôleur de la fenêtre 'à propos...'
 * 
 * @author RomainPETIT
 */
public class CaProposForm extends Tcontroller implements Initializable {
    @FXML
    private Label version;

    /**
     * Initializes the controller class.
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public final void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    /**
     * Rafraîchit l'affichage
     */
    @Override
    public final void refresh() {
        version.setText("v"+getForm().getApplication().getVersion());
    }

    /**
     * Initialisation différée
     */
    @Override
    public final void manualIni() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
