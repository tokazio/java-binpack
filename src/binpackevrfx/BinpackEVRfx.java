/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package binpackevrfx;

import esyoLib.Formsfx.Tapplication;
import esyoLib.Formsfx.Tform;
import esyoLib.Files.TfileUtils;
import evrLib.gc.TgcOptions;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Application pour le binpacking.
 * 
 * Passer en paramètre le dossier où trouver les fichiers de configuration *.pla
 * Il seront tous listés.
 * 
 * Ceux selectionnés seront cumulés pour créer les listings des découpes.
 * 
 * @author rpetit
 */
public class BinpackEVRfx extends Application {
    /**
     * Options
     */
    public static TgcOptions options;
    
    /**
     * Application
     */
    public static Tapplication application;
    
    /**
     * 
     * @param primaryStage Stage principal
     */
    @Override
    public void start(Stage primaryStage){
        //nouvelle application (esyo) EN PREMIER!!
        application=new Tapplication(primaryStage, this);
        application.setIco("ico.jpg");
        //options
        try {
            options=new TgcOptions("options.txt",this.getClass());
        } catch (IOException | IllegalArgumentException | IllegalAccessException ex) {
            application.showError(ex.getClass().getName(), ex.getMessage());
        }
        //Nouvelle fenêtre avec son titre
        Tform f=application.createForm("Main","binpackevrfx/mainForm.fxml",null).setTitle("Binpack");
        //Cette fenêtre est la fenêtre principale
        application.setMainForm(f);
        //
        ((CmainForm)f.getController()).manualIni();
        //Affiche la fenêtre
        f.show();
        //paramètres
        if(getParameters().getRaw().size()>0){
            System.out.println("Paramètres: "+getParameters().getRaw().toString());
            //D:\EVR BE\TECHNIQUE\CHANTIERS\GAMME MONOPROFIL\2014\PESCE\PESCE-1.pla
            String s=TfileUtils.extractFilePath(getParameters().getRaw().get(0),"\\\\");
            try {
                ((CmainForm)f.getController()).open(s);
            } catch (IOException ex) {
                application.showError("Error", "Impossible d'ouvrir le dossier "+s);
            } catch (Exception ex) {
                application.showError("Error", "Erreur de calcul à l'ouverture de "+s);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
