/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binpackevrfx;

import evrLib.gc.Tbarre;
import evrLib.binpack.Tmorceau;
import evrLib.binpack.Tbinpacker;
import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tvue2D;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Vue des découpes selon référence du profilé
 *
 * @author rpetit
 */
public class TvueBinpack extends Tvue2D {

    //Référence du profilé
    private String ref;
    //Début de la plage de barre à dessiner dans cette vue (dans la liste complète des barres)
    private int DEB;
    //Fin de la plage de barre à dessiner dans cette vue (dans la liste complète des barres)
    private int FIN;
    //
    private Tbinpacker binpacker;

    /**
     * Constructeur
     *
     * @param aRef
     * @param aBinpacker
     * @throws esyoLib.Formsfx.TformException
     */
    public TvueBinpack(String aRef,Tbinpacker aBinpacker) throws TformException {
        super();
        this.binpacker=aBinpacker;
        this.ref = aRef;
        this.DEB = 0;
        this.FIN = 0;
    }

    /**
     * Défini la plage de barre à dessiner dans cette vue
     *
     * @param start Position de départ de la plage (dans la liste complète des
     * barres)
     * @param length Taille
     */
    public final void setRANGE(int start, int length) {
        this.DEB = start;
        this.FIN = start + length;
    }

    /**
     * Crée la fenêtre pour la vue
     *
     * @param f fenêtre Tform
     */
    @Override
    public final void onCreateForm(Tform f) {
        f.setSize(mmToPxScreen(210), mmToPxScreen(297)).setTitle(this.titleView()).show();
    }

    /**
     * 
     * @return 
     */
    @Override
    public String titleView() {
        return "Découpes de " + this.ref;
    }

    /**
     * 
     * @throws Exception 
     */
    @Override
    public void draw() throws Exception {
        if (this.FIN > this.binpacker.getBarres().size()) {
            this.FIN = this.binpacker.getBarres().size();
        }
        double startX = 0;
        double startY = 0;
        double margeX = textWidth(" 999 ");
        double margeY = textHeight("1");
        double x = startX + margeX;
        double y = startY + margeY;
        double h = textHeight("p'");
        double th = margeY * 2 + Math.round((this.binpacker.getNombreDeBarres() * (h + 20)) + (7 * h));
        DecimalFormat df = new DecimalFormat("#.00");
        double c = (getCanvasWidth() - margeX - margeX - startX - startX) / this.binpacker.getLGBARRE();
        //
        getGC().setFont(new Font(10));
        // efface
        clear(Color.WHITE);
        //
        style.normal();

        leftText("Chantier: " + this.binpacker.getNom(), x, y);
        y += 2 * h;
        leftText("Binpacker v" + BinpackEVRfx.application.getVersion() + " le " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " (" + this.binpacker.getDefText() + ")", x, y);
        y += 2 * h;
        String ma = "";
        if (this.binpacker.getMarge() > 0) {
            ma = "Marge de " + this.binpacker.getMarge() + "mm.";
        }
        leftText(this.binpacker.getNombreDeMorceaux() + " morceaux dans " + this.binpacker.getNombreDeBarres() + " barres de " + df.format(this.binpacker.getLGBARRE() / 1000) + "m soit " + df.format((this.binpacker.getLGBARRE() / 1000) * this.binpacker.getNombreDeBarres()) + "m. " + ma, x, y);
        y += 2 * h;

        for (int I = this.DEB; I < this.FIN; I++) {
            leftText("" + (I + 1), x - textWidth("999 "), y + 14);
            getGC().setFill(Color.CORAL);
            rectangle(x, y, Math.round(x + (this.binpacker.getLGBARRE() * c)) + 2, y + h + 12);
            for (int J = 0; J < ((Tbarre) this.binpacker.getBarres().get(I)).getMorceaux().size(); J++) {
                double w = Math.round(((Tmorceau) ((Tbarre) this.binpacker.getBarres().get(I)).getMorceaux().get(J)).getLongueur() * c);
                getGC().setFill(Color.GREENYELLOW);
                rectangle(x, y, x + w, y + h + 12);
                Tmorceau m = ((Tmorceau) ((Tbarre) this.binpacker.getBarres().get(I)).getMorceaux().get(J));
                //longueur ligne 1
                String t = df.format(m.getLongueur());
                leftText(t, x + 5, y + 8);
                //famille ligne 2
                if (!m.getFamille().isEmpty()) {
                    leftText(m.getFamille(), x + 5, y + 20);
                }

                if (!m.getType().isEmpty()) {
                    leftText(m.getType(), x + textWidth(t) + 5, y + 8);
                    t += " " + m.getType();
                }

                if (!m.getPortion().isEmpty()) {
                    getGC().strokeOval(x + textWidth(t) + 8 - 6, y + 8 - 6, 13, 13);
                    centerText(m.getPortion(), x + textWidth(t) + 8, y + 8);
                }
                x += w;
            }
            //canvas.brush.Style := bsClear;
            leftText(df.format(((Tbarre) this.binpacker.getBarres().get(I)).getChute()), x + 5, y + 8);
            // ligne suivante
            y += h + 20;
            // morceau 0
            x = startX + margeX;
        }
    }

    @Override
    public void onDragged(MouseEvent e) {
        
    }

    @Override
    public void onClick(MouseEvent e) {
        
    }

    @Override
    public void onDblClick(MouseEvent e) {
        
    }

    @Override
    public void onPressed(MouseEvent e) {
        
    }

    @Override
    public void onReleased(MouseEvent e) {
        
    }

    @Override
    public void onMove(MouseEvent e) {
        
    }

}
